/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   ScramblerTest.cpp
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Implementation of Scrambler Unit Test.
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include "ScramblerTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(ScramblerTest);

ScramblerTest::ScramblerTest() {
    // no-op
}

ScramblerTest::~ScramblerTest() {
    // no-op
}

void ScramblerTest::setUp() {
    scrambler = csc232::Scrambler{INIT_DATA, FIRST_SLOT, NEXT_FREE_SLOT};
}

void ScramblerTest::tearDown() {
    // no-op
}

void ScramblerTest::testLength() {
    size_t expected{EXPECTED_LENGTH};
    size_t actual{scrambler.length()};

    CPPUNIT_ASSERT_EQUAL(expected, actual);
}

void ScramblerTest::testScramble() {
    std::string expected{EXPECTED_SCRAMBLED_TEXT};
    std::string actual{scrambler.scramble()};
    CPPUNIT_ASSERT_EQUAL(expected, actual);
}

void ScramblerTest::testToString() {
    std::string expected{EXPECTED_ORIGINAL_TEXT};
    std::string actual{scrambler.toString()};
    CPPUNIT_ASSERT_EQUAL(expected, actual);
}
