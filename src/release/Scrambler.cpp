/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Scrambler.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Scrambler implementation.
 *
 * @copyright Jim Daehn, 2017. All rights reserved
 */


#include <sstream>

#include "Scrambler.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    Scrambler::Scrambler(const char a[], index beginIndex, index endIndex)
            : begin(beginIndex), end(endIndex) {
        // TODO: Copy the given array into the data member array defined by
        // the given boundaries.
    }

    size_t Scrambler::length() const {
        // TODO: Compute me correctly.
        return 0;
    }

    std::string Scrambler::scramble() const {
        // TODO: Use reverse() correctly to obtain the data in reverse.
        // You are tasked with determined what the arguments need to be for
        // correct operation. You are free to declare any local variables to
        // make that happen.
        return "";
    }

    std::string Scrambler::toString() const {
        // TODO: Create a std::string representation of the character data
        // stored by this Scrambler.
        return "";
    }


    Scrambler::~Scrambler() {
        // No op... leave me alone.
    }

    // Private data member implementations
    // Compare/contrast this with writeArrayBackward on page 67.
    void Scrambler::reverse(const char anArray[], std::stringstream& ss,
                            const int first, const int last) const {
        // TODO: Implement me.
    }
}
