/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Descrambler.cpp
 * @author Jim Daehn <jrd2112@missouristate.edu>
 * @brief  Descrambler implementation.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <iostream>
#include <iterator>

#include "Descrambler.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    Descrambler::Descrambler(const char *a, index beginIndex, index endIndex)
            : data{std::string{a}.substr(beginIndex, endIndex - beginIndex)} {
        // no-op
    }

    size_t Descrambler::length() const {
        return data.size();
    }

    std::string Descrambler::unscramble() const {
        return std::string{data.crbegin(), data.crend()};
    }

    std::string Descrambler::toString() const {
        return data;
    }

    Descrambler::~Descrambler() {
        // no-op
    }
}
