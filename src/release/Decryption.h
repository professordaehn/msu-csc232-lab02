/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Decryption.h
 * @author Jim Daehn <jrd2112@missouristate.edu>
 * @brief  Decryption specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef LAB02_DECRYPTION_H
#define LAB02_DECRYPTION_H

#include <string>

#include "Crypto.h"

/**
 * @brief   Encapsulates ADTs developed in CSC232, Data Structures with C++.
 * @extends csc232::Crypto
 */
namespace csc232 {
    /**
     * @brief Abstract base class for decryption classes of the Crypto class
     *        hierarchy.
     */
    class Decryption : public Crypto {
        /**
         * Return an "unscrambled" form of the characters stored in this
         * Decryption.
         *
         * @return A string form of the scrambled character data given to this
         *         Decryption, unscrambled, is returned.
         * @post   The encrypted data stored by this Decryption remains
         *         unchanged.
         */
        virtual std::string unscramble() const = 0;

        /**
         * @brief  An accessor method for the length of data stored in this
         *         Decryption instance.
         * @return The length of the data stored by this Decryption instance is
         *         returned.
         * @post   The state of this Decryption instance remains unchanged after
         *         executing this operation.
         */
        virtual size_t length() const override = 0;

        /**
         * Return a string representation of the character data stored by this
         * Decryption instance.
         *
         * @return A string representation of the character data stored by this
         *         Decryption is returned.
         * @post   The state of this Decryption intance remains unchanged
         *         after executing this operation.
         */
        virtual std::string toString() const = 0;
    };
}
#endif //LAB02_DECRYPTION_H
